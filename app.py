import psycopg2
import os

host = os.getenv('HOST')
pw = os.getenv('PASSWORD')

conn = psycopg2.connect(
    database="dev",
    user="awsuser",
    host=host,
    password=pw,
    port=5439
)

cur = conn.cursor()
cur.execute("""select distinct nspname from pg_namespace""")

records = cur.fetchall()
print(records)
cur.close()
conn.close()
